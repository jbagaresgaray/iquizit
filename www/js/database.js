var gameArr = new Array();
var score = 0;
var indexquestion = 0;
var countAnswer = 0;
var questionArray = [];
var sSQL = "";
var db = window.openDatabase("quizdb", "1.0", "iQuizIT", 1000000);

document.addEventListener("deviceready", onDeviceReady, false);


function onDeviceReady() {
    // if (db != null && db != undefined && db != "") {
    db.transaction(populatedb, errorCB, successCB);
    // }
}

$(document).ready(function() {
    // if (db != null && db != undefined && db != "") {
    db.transaction(populatedb, errorCB, successCB);
    // }
});


function successCB() {
    console.log("success!");
    // db.transaction(queryDB, errorCB);
}

function showinput() {
    $('#popupScore').popup('open');
}

function closeInput(transaction, resultSet) {
    $('#popupScore').popup('close');
    console.log('Query completed: ' + JSON.stringify(resultSet));
    // home();

}

function home() {
    window.location.href = 'index.html#type';
}

function excel_tutorial() {
    window.location.href = 'excel.html';
}

function word_tutorial() {
    window.location.href = 'word.html';
}

function word_gameload() {
    window.location.href = 'quiz/word_quiz.html';
};

function power_tutorial() {
    window.location.href = 'power.html';
}

function software_tutorial() {
    window.location.href = 'software.html';
}

function os_tutorial() {
    window.location.href = 'operating.html';
}

function browser_tutorial() {
    window.location.href = 'browser.html';
}

function comp_parts() {
    window.location.href = 'parts.html';
}

function config_tutorial() {
    window.location.href = 'config.html';
}

function specs_tutorial() {
    window.location.href = 'specs.html';
}

function goToHP() {
    window.location.href = 'index.html';
}

function closeApp() {
    console.log('closeApp');
    navigator.app.exitApp();
}

function leaderboard() {
    window.location.href = 'score.html';
}

function gameload3() {
    window.location.href = 'game2.html';
}

function endgameload() {
    var itemscore = sessionStorage.getItem('yourscore');
    $("#game_score").innerHTML = itemscore;
}


function errorCDB(err) {
    var db = window.openDatabase("quizdb", "1.0", "iQuizIT", 1000000);
    db.transaction(CreateQuestions, errorCB, CreateQuestSuccessFul);
}

function errorCB(err) {
    console.log("Error processing SQL: " + err.code + ' ' + err.message);
}

function CreateQuestSuccessFul() {
    var db = window.openDatabase("quizdb", "1.0", "Quiz Database", 1000000);
    db.transaction(queryDB, errorCB);
}

function gameload(param, slesson) {

    localStorage.clear();

    window.localStorage.setItem('lesson', slesson);

    console.log('param: ', param);

    if (param == 'word') {
        sSQL = 'SELECT * FROM quiztbl WHERE lesson_id="4";';
        window.localStorage.setItem("sSQL", sSQL);
        window.localStorage.setItem("param", param);
        window.location.href = 'word_quiz.html';
    } else if (param == 'excel') {
        sSQL = 'SELECT * FROM quiztbl WHERE lesson_id="5";';
        window.localStorage.setItem("sSQL", sSQL);
        window.localStorage.setItem("param", param);
        window.location.href = 'excel_quiz.html';
    } else if (param == 'power') {
        sSQL = 'SELECT * FROM quiztbl WHERE lesson_id="6";';
        window.localStorage.setItem("sSQL", sSQL);
        window.localStorage.setItem("param", param);
        window.location.href = 'power_quiz.html';
    } else if (param == 'software') {
        sSQL = 'SELECT * FROM quiztbl WHERE lesson_id="7";';
        window.localStorage.setItem("sSQL", sSQL);
        window.localStorage.setItem("param", param);
        console.log("window.location.href = 'software_quiz.html 1'");
        window.location.href = 'software_quiz.html';
    } else if (param == 'browser') {
        sSQL = 'SELECT * FROM quiztbl WHERE lesson_id="9";';
        window.localStorage.setItem("sSQL", sSQL);
        window.localStorage.setItem("param", param);
        window.location.href = 'browser_quiz.html';
    } else if (param == 'os') {
        sSQL = 'SELECT * FROM quiztbl WHERE lesson_id="8";'
        window.localStorage.setItem("sSQL", sSQL);
        window.localStorage.setItem("param", param);
        window.location.href = 'operating_quiz.html';
    } else if (param == 'parts') {
        sSQL = 'SELECT * FROM quiztbl WHERE lesson_id="1";'
        window.localStorage.setItem("sSQL", sSQL);
        window.localStorage.setItem("param", param);
        window.location.href = 'parts_quiz.html';
    } else if (param == 'config') {
        sSQL = 'SELECT * FROM quiztbl2 WHERE lesson_id="2";'
        window.localStorage.setItem("sSQL", sSQL);
        window.localStorage.setItem("param", param);
        window.location.href = 'config_quiz.html';
    } else {
        var value = sessionStorage.getItem('param');
        if (value == 'word') {
            sSQL = 'SELECT * FROM quiztbl WHERE lesson_id="4";';
            window.localStorage.setItem("sSQL", sSQL);
            window.localStorage.setItem("param", value);
            window.location.href = 'word_quiz.html';
        } else if (value == 'excel') {
            sSQL = 'SELECT * FROM quiztbl WHERE lesson_id="6";';
            window.localStorage.setItem("param", value);
            window.localStorage.setItem("sSQL", sSQL);
            window.location.href = 'excel_quiz.html';
        } else if (value == 'power') {
            sSQL = 'SELECT * FROM quiztbl WHERE lesson_id="5";';
            window.localStorage.setItem("sSQL", sSQL);
            window.localStorage.setItem("param", value);
            window.location.href = 'power_quiz.html';
        } else if (value == 'software') {
            sSQL = 'SELECT * FROM quiztbl WHERE lesson_id="7";';
            window.localStorage.setItem("sSQL", sSQL);
            window.localStorage.setItem("param", value);
            console.log("window.location.href = 'software_quiz.html 2'");
            window.location.href = 'software_quiz.html';
        } else if (value == 'browser') {
            sSQL = 'SELECT * FROM quiztbl WHERE lesson_id="9";';
            window.localStorage.setItem("sSQL", sSQL);
            window.localStorage.setItem("param", value);
            window.location.href = 'browser_quiz.html';
        } else if (value == 'os') {
            sSQL = 'SELECT * FROM quiztbl WHERE lesson_id="8";'
            window.localStorage.setItem("sSQL", sSQL);
            window.localStorage.setItem("param", value);
            window.location.href = 'operating_quiz.html';
        } else if (param == 'parts') {
            sSQL = 'SELECT * FROM quiztbl WHERE lesson_id="1";'
            window.localStorage.setItem("sSQL", sSQL);
            window.localStorage.setItem("param", value);
            window.location.href = 'parts_quiz.html';
        } else if (param == 'config') {
            sSQL = 'SELECT * FROM quiztbl2 WHERE lesson_id="2";'
            window.localStorage.setItem("sSQL", sSQL);
            window.localStorage.setItem("param", param);
            window.location.href = 'config_quiz.html';
        }
    }
}

function saveScore(category) {
    if ($('#un').val() == '') {
        alert('Please enter you Name');
        return;
    }

    var lesson = window.localStorage['lesson'];

    db.transaction(function(tx) {
        var sSQL = 'INSERT INTO scores(player_name,score,lesson,category) VALUES ("' + $('#un').val() + '","' + $('#game_score').text() + '","' + lesson + '","' + category + '")';

        console.log(sSQL);

        tx.executeSql(sSQL, [], closeInput, errorCB);

        alert('Score successfully saved');

    }, errorCB);

}

function populatedb(tx) {

    // tx.executeSql('DROP TABLE IF EXISTS scores');
    tx.executeSql('DROP TABLE IF EXISTS category');
    tx.executeSql('DROP TABLE IF EXISTS lesson');
    tx.executeSql('DROP TABLE IF EXISTS quiztbl');
    tx.executeSql('DROP TABLE IF EXISTS quiztbl2');
    tx.executeSql('DROP TABLE IF EXISTS quiztbl3');

    tx.executeSql('CREATE TABLE IF NOT EXISTS scores(s_id INTEGER PRIMARY KEY AUTOINCREMENT, player_name VARCHAR(255),score VARCHAR(25),lesson text,category VARCHAR(45))');
    tx.executeSql('CREATE TABLE IF NOT EXISTS category(c_id INTEGER PRIMARY KEY AUTOINCREMENT, c_name VARCHAR(150))');
    tx.executeSql('CREATE TABLE IF NOT EXISTS lesson(lesson_id INTEGER PRIMARY KEY AUTOINCREMENT, c_id INTEGER REFERENCES category (c_id) ON DELETE CASCADE ON UPDATE CASCADE, lesson_detail TEXT)');
    tx.executeSql('CREATE TABLE IF NOT EXISTS quiztbl (ID INTEGER PRIMARY KEY AUTOINCREMENT,lesson_id INTEGER REFERENCES lesson (lesson_id) ON DELETE CASCADE ON UPDATE CASCADE, question text, qanswer1 text, qanswer2 text, qanswer3 text,qanswer4 text, answer text)');
    tx.executeSql('CREATE TABLE IF NOT EXISTS quiztbl2 (ID INTEGER PRIMARY KEY AUTOINCREMENT,lesson_id INTEGER REFERENCES lesson (lesson_id) ON DELETE CASCADE ON UPDATE CASCADE, question text, qanswer1 text, qanswer2 text, answer text)');
    tx.executeSql('CREATE TABLE IF NOT EXISTS quiztbl3 (ID INTEGER PRIMARY KEY AUTOINCREMENT,lesson_id INTEGER REFERENCES lesson (lesson_id) ON DELETE CASCADE ON UPDATE CASCADE, question text, qanswer1 text, qanswer2 text, qanswer3 text, answer text)');

    tx.executeSql('INSERT INTO category(c_name) VALUES("Hardware")');
    tx.executeSql('INSERT INTO category(c_name) VALUES("Software")');

    tx.executeSql('INSERT INTO lesson(c_id,lesson_detail) VALUES ("1","Computer Parts and Functions")');
    tx.executeSql('INSERT INTO lesson(c_id,lesson_detail) VALUES ("1","Basic Hardware Configuration Setup")');
    tx.executeSql('INSERT INTO lesson(c_id,lesson_detail) VALUES ("1","Computer Hardware Specifications")');

    tx.executeSql('INSERT INTO lesson(c_id,lesson_detail) VALUES ("2","Microsoft Word 2010")');
    tx.executeSql('INSERT INTO lesson(c_id,lesson_detail) VALUES ("2","Microsoft Excel 2010")');
    tx.executeSql('INSERT INTO lesson(c_id,lesson_detail) VALUES ("2","Microsoft PowerPoint 2010")');
    tx.executeSql('INSERT INTO lesson(c_id,lesson_detail) VALUES ("2","Software Fundamental")');
    tx.executeSql('INSERT INTO lesson(c_id,lesson_detail) VALUES ("2","The Operating System")');
    tx.executeSql('INSERT INTO lesson(c_id,lesson_detail) VALUES ("2","The Web Browser")');

    //PARTS
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("1","What is the temporary memory built in your computer? It is also called as short-term memory.", "RAM", "ROM", "CPU", "All of the above","RAM")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("1","Which of the following is NOT computer hardware?", "Mouse", "Monitor", "Antivirus", "Keyboard","Antivirus")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("1","What is the main purpose of secondary device?", "To increase the speed of computer", "To store data", "To Networking", "To decrease the speed of the computer","To store data")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("1","Cables that are used to set up local area networks.", "RJ11", "Ethernet", "Hard drive", "USB cable","Ethernet")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("1","Acts as a manager for everything on the computer – connects all the other components together.", "CPU", "Hard drive", "Motherboard", "Computer casing","Motherboard")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("1","What does SATA Stands for?", "Serial Advanced Technology Attachment", "Service Attachment Technology Adapter", "Storage Advance Technology Attachment", "Serial Adaptor Technology Advance","Serial Advanced Technology Attachment")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("1","Main circuit board in a computer", "Decoder", "Highlight", "Motherboard", "None of these","Motherboard")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("1","What does ROM Stands For?", "Read Only Message", "Read Only Memory", "Repair Operations Manual", "All of these","Read Only Memory")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("1","Acts as a manager for everything on the computer – connects all the other components together.", "CPU", "Hard drive", "Motherboard", "Computer casing","Motherboard")');

    //WORD
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","A tab allows you to change the view of your document to a different two page document or zoom.", "View tab", "Home tab", "Insert tab", "Review tab","View tab")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","These tools are used to change the way that you view your document.", "Window", "Document Views", "Show", "Macro","Document Views")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","These tools can be used to protect your document from being edited by other people.", "Proofing", "Tracking", "Comments", "Protect","Protect")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","This tab allows you to make any changes to your document due to spelling and grammar issues.", "View tab", "Review tab", "Mailing tab", "References tab","Review tab")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","This tab allows you to create documents to help when sending out mailings such as printing envelopes, labels and processing mail merges.", "References tab", "Mailing tab", "Review tab", "View tab","Mailing tab")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","This tab has commands to use when creating a Table of Contents and citation page for a paper.", "References tab", "Mailing tab", "Review tab", "View tab","References tab")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","It is a word-processing program, designed to help you create professional-quality documents.", "Microsoft Word", "Microsoft Excel", "Microsoft PowerPoint", "Microsoft Access","Microsoft Word")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","It is a buttons and tools affect the way that characters and words appear on the printed document.", "Clipboard", "Font", "Paragraph", "Styles","Font")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","This tab allows you to insert a variety of items into a document from pictures, clip art, tables and headers and footers.", "Home tab", "Insert tab", "Page Layout tab", "References tab","Insert tab")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","These tools and buttons are used to insert specific characters that may not appear on your keyboard into your document.", "Text", "Font", "Symbols", "Illustrations","Symbols")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","Which bar is usually located below that Title Bar that provides categorized option?", "Menu Bar", "Status Bar", "Tool Bar", "Scroll Bar","Menu Bar")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","Which of these toolbars allows changing fonts and their sizes?", "Standard", "Formatting", "Print View", "None of the above","Formatting")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","Which menu is MS-Word can be used to change characters size and typeface?", "View", "Tools", "Format", "Data","Format")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","In MS-Word, for what does ruler help?", "To set tabs", "To set indents", "To change page margins", "Any of the above","Any of the above")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","MS-Word automatically moves the text to the next line when it reaches the right edge of the screen and is called?", "Carriage Return", "Enter", "Word Wrap", "None of the above","Word Wrap")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","Selecting text means, selecting?", "A word", "An entire sentence", "Whole document", "Any of the above","Any of the above")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","Which option in File pull-down menu is used to close a file in MS-Word?", "New", "Quit", "Close", "Exit","Close")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","Which key should be pressed to start a new paragraph of MS-Word?", "Enter key", "Ctrl key", "Shift key", "Space key","Enter key")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","These buttons and tools can be used to track any and all changes that you make to your document.", "Marcos", "Captions", "Protect", "Tracking","Tracking")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("4","What is the most used tab of MS-Word?", "View", "Home", "Page Layout", "Insert","Insert")');

    //EXCEL
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","An Excel Workbook is a collection of?", "Workbooks", "Worksheets", "Charts", "Worksheets and Charts","Worksheets and Charts")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","What do you mean by a workspace?", "Group of Columns", "Group of Worksheets", "Group of Row", "Group of Workbooks","Group of Workbooks")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","MS-Excel is based on?", "Windows", "DOS", "UNIX", "OS/2","Windows")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","Which is not the function of Edit, Clear Command?", "Delete Contents", "Deletes Notes", "Delete Cells", "Delete Formats","Delete Cells")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","Microsoft Excel is a powerful?", "Word processing Package", "Spreadsheet Package", "Communication Package", "DBMS Package","Spreadsheet Package")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","How do you rearrange the data in ascending or descending order?", "Data, Sort", "Data, Form", "Data, Table", "Data, Subtotals","Data, Sort")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","Multiple calculations can be made in a single formula using?", "Standard Formula", "Array Formula", "Complex Formula", "Smart Formula","Array Formula")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","Comment put in cells are called?", "Smart Tip", "Cell Tip", "Web Tip", "Soft Tip","Cell Tip")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","Which area in an Excel window allows entering values and formulas?", "Title Bar", "Menu Bar", "Formula Bar", "Standard Tool Bar","Formula Bar")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","In Excel, you can sum large range of data by simply selecting a tool button called?", "Auto Fill", "Auto Correct", "Auto Sum", "Auto","Auto Sum")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","Which of the following is not a valid data type in Excel?", "Number", "Character", "Label", "Date/Time","Label")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","To select an entire column in MS-Excel, press?", "Ctrl + C", "Ctrl + P", "Alt + A", "None of the above","None of the above")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","To return the remainder after a number is divided by a division in Excel we use the function?", "ROUND()", "FACT()", "MOD()", "DIV()","MOD()")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","Which mode enables you to use the mouse to select cells for use in a formula?", "Edit mode", "Formula mode", "Selection mode", "Point mode","Point mode")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","Which option button provides formatting options following the insertion of cells, rows, or columns in a worksheet?", "AutoCorrect", "Paste", "Insert", "Auto Fill","Insert")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","Which set of formatting choices includes a set of colors, fonts, and effects that you can apply to a worksheet to enhance its appearance?", "Layout", "Theme", "Template", "Design","Theme")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","Which ribbon contains the command to apply a theme to a worksheet in Excel 2010?", "Home", "Data", "Page Layout", "View","Page Layout")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","Where can you find the command to apply a background color to a cell or range of cells in a worksheet?", "Home Ribbon", "Page Layout Ribbon", "Insert Ribbon", "View Ribbon","Home Ribbon")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","Which dialog box contains the commands to specify an exact date and time format to use for a selected cell in a worksheet?", "Number", "Paragraph", "Font", "Format Cells","Format Cells")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("5","Where is the spellchecker located?", "Home Ribbon", "Review Ribbon", "Page Layout Ribbon", "View Ribbon","Page Layout Ribbon")');

    //POWERPOINT
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","Which set of formatting choices includes a set of colors, fonts, and effects that you can apply to a   worksheet to enhance its appearance?", "Layouts", "Templates", "Themes", "Designs","Themes")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","Power Point presentation is a collection of?", "Slides and Handouts", "Outlines", "Speaker’s Notes", "All of the above","All of the above")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","From where can we set the timing for each object?", "slide show, custom transition", "slide show, custom animation", "Slide show, Slide transition", "view, slide sorter","slide show, custom animation")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","The arrangement of elements such as Title and subtitle text, pictures, tables etc. is called.", "Layout", "Design", "Presentation", "Scheme","Layout")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","Ellipse Motion is a predefined?", "Design Template", "Animation Scheme", "Color Scheme", "None of these","Animation Scheme")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","How can the Slide show be repeatedly continuously?", "Loop continuously until ESC", "Repeat continuously", "Loop more", "None","Loop continuously until ESC")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","After choosing a predefined template which option has to be chosen to change the background color?", "Design Template", "Color Scheme", "Animation Scheme", "Color Effects","Color Scheme")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","A file which contains readymade styles that can be used for a presentation is called:", "Auto Style", "Template", "Wizard", "Pre-formatting","Template")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","Animation Scheme can be applied to ______ in the presentation.", "All Slides", "Select Slide", "Current Slide", "All of the above","All of the above")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","Slide can have?", "Title, Text, Graphs", "Drawn, Objects, Shapes", "Clip Art, Drawn Art, Visual", "Any of the above","Any of the above")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","We can replace a font on all slides with another font using the option:", "Edit -> Fonts", "Tools -> Fonts", "Tools -> Replace Fonts", "Format -> Replace Fonts","Format -> Replace Fonts")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","Which key on the keyboard can be used to view Slide Show?", "F10", "F5", "F2", "F1","F5")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","Which view in PowerPoint can be used to enter Speaker Comments?", "Normal", "Slide Show", "Slide Sorter", "Notes Page View","Notes Page View")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","Which option can be used to set custom timings for slides in a presentation?", "Slider Timings", "Slide Timer", "Rehearsal", "Slide Show Setup","Rehearsal")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","Which option can be used to create a new slide show with a correct slide but presented in a different order?", "Rehearsal", "Custom Slide Show", "Slide Show Setup", "Slide Show View","Custom Slide Show")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","The spelling dialog box can be involved by choosing spelling from _____ menu?", "Insert", "File", "Tools", "View","Toals")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","Slide sorter of PowerPoint is available on _____ menu?", "Insert", "Tools", "Edit", "View","Tools")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","What is a Slide Transition?", "Over heads", "Letters", "A special effect used to introduced a slide in a slide", "The way one slide looks","A special effect used to introduced a slide in a slide")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","What is the most commonly used tab in MS PowerPoint are also the most accessible.", "Home tab", "Design tab", "Transition tab", "Animation tab","Home tab")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("6","What Microsoft Office Suite that enables you to present information in office meetings, lectures and seminars to create maximum impact in a minimal amount of time?", "Microsoft Access","Microsoft PowerPoint","Microsoft Excel", "Microsoft Word","Microsoft PowerPoint")');

    //SOFTWARE FUNDAMENTAL
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","This type of software works with end-users, application software, and computer hardware to handle the majority of technical details.", "Communications Software", "Application Software", "Utility Software", "System Software","System Software")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","Software that enables you to perform specific information-processing tasks is referred to as?", "Communications Software", "Application Software", "Utility Software", "System Software","Application Software")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","General-purpose applications include all of the following except:", "Web Authoring", "Word Processors", "Spreadsheet", "Database Management Systems","Web Authoring")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","Word processors and spreadsheets are examples of?", "Advanced Applications", "General Purpose Applications", "Special Purpose Applications", "Virtual Reality Applications","General Purpose Applications")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","Software can be described as end-user software designed to accomplish a variety of different tasks.", "Application Software", "System Software", "Utility Software", "None","Application Software")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","Software that developed to harm and disrupt computers.", "Utility Software", "Device Driver", "System Software", "Malicious Software","Malicious Software")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","Refers to the non-tangible components of computers, known as computer programs.", "Hardware", "Output", "Software", "Input","Software")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","A computer program that operates or controls a particular type of device that is attached to a computer.", "Device Driver", "Application Suites", "Hardware", "Malware","Device Driver")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","It provides common capabilities needed to support enterprise software systems.", "Application Suites", "Simulation Software", "Enterprise Infrastructure Software", "Enterprise Software","Enterprise Infrastructure Software")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","It is used in developing hardware and software products.", "Simulation Software", "Educational Software", "Enterprise Infrastructure Software", "Product Engineering Software","Product Engineering Software")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","Usually consists of multiple applications bundled together is?", "Application Suites", "Enterprise Software", "Enterprise infrastructure software", "Information worker software","Application Suites")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","A software designed to assist users in maintenance and care of their computers?", "Application Software", "System Software", "Utility Software", "None","Utility Software")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","This type of software primarily used to access content without editing, but may include software that allows for content editing?", "Product Engineering Software", "Content access software", "General Purpose Applications", "Enterprise Infrastructure Software","Content access software")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","This application software simulates physical or abstract systems for research, training or purposes.", "Simulation Software", "Educational Software", "Enterprise Infrastructure Software", "Product Engineering Software","Simulation Software")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","This type of software is an essential collection of computer programs that manages resources and provides common services for other software programs.", "System Software", "Application Software", "Operating System", "Malicious Software","Operating System")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","Which of the following program is not a utility?", "Debugger", "Spooler", "Editor", "All of the above","Spooler")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","Which is an example of Content Access Software? ", "Web Browsers", "Media Players", "Help Browsers", "All of the above","All of the above")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","Which is NOT an example of Application Suites? ", "Microsoft Office", "Web Browser", "iWork", "Libre Office","Web Browser")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","Which is NOT an example of System Software?", "Device Driver", "Utilities Software", "Malicious Software", "Operating System","Malicious Software")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("7","Which is NOT an example of Media Development Software?", "Computer-aided Engineering", "Digital Audio", "Digital-animation Editors", "HTML Editors","Computer-aided Engineering")');

    //OPERATING SYSTEM
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("8","Who is called a supervisor of computer activity?", "Memory", "Operating System", "I/O Devices", "Control Unit","Operating System")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("8","Operating System manages?", "Memory", "Processor", "I/O Devices", "All of the above","All of the above")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("8","UNIX operating system is an?", "Multi-user OS", "Time-sharing OS", "Multi-tasking OS", "All of the above","All of the above")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("8","_____ shares characteristics with both hardware and software?", "Operating System", "Software", "Data", "None","Operating System")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("8","Which is the layer of a computer system between the hardware and the user program?", "Operating environment", "Operating System", "System environment", "None","Operating System")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("8","Which runs on computer hardware and serve as platform for other software to run on?", "Operating System", "Application Software", "System Software", "All","Operating System")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("8","The primary purpose of an Operating System is:", "To make the most efficient use of the computer hardware", "To allow people to use the computer", "To keep the programmers employed", "None","To make the most efficient use of the computer hardware")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("8","An operating system based on the Linux kernel, and designed primarily for touchscreen mobile devices such as smartphones and tablet computers is?", "Windows Phone", "Symbian", "Android", "Google Chromium OS","Android")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("8","It is a series of proprietary smartphone operating systems developed by Microsoft.", "Microsoft Windows", "Windows Phone OS", "Android OS", "Symbian","Windows Phone OS")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("8","Which is built directly on the hardware?", "Computer Environment", "Application Software", "Operating System", "Database System","Operating System")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("8","What is operating system?", "Collection of programs that manages hardware resources", "System service provider to the application programs", "Link to interface the hardware and application programs", "All of the mentioned","All of the mentioned")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("8","An example of an operating system is?", "Windows XP", "UNIX or LINUX", "MAC OS", "all of the above","all of the above")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("8","What operating system based on the Linux kernel and designed by Google?", "Android", "Google Chromium OS", "Microsoft Windows", "UNIX","Google Chromium OS")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("8","What OS support pre-emptive multitasking?", "Android", "Google Chromium OS", "Microsoft Windows", "UNIX-like OS","UNIX-like OS")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("8","Type of OS allows multiple users to access a computer system at the same time.", "Multi-user", "Real-time", "Embedded", "Distributed","Multi-user")');

    //WEB BROWSER
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("9","A software application for retrieving, presenting and traversing information resources on the World Wide Web.", "Web Browser", "WAMP Server", "Application Suites", "PHP","Web Browser")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("9","It is developed by Avant Force with the initial launch in 2004. The browser can supports Gecko and WebKit based layout engines.", "Netsurf", "Maxthon", "Avant Browser", "Google Chrome","Avant Browser")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("9","It is developed by The NetSurf Developers with its stable release in 2012. It has its own layout engine and is built for AmigaOS, Atari OS, Mac OS X, RISC OS, Unix-like platforms and a few third party ports as well.", "Web", "Maxthon", "Netsurf", "Voyager","Netsurf")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("9","It is developed by Maxthon International Limited for Microsoft Windows, Mac OS X and iOS along with Android for Maxthon mobiles. The browser is recently upgraded for 10 inch tablets in 2011. The latest Maxthon 3 version supports WebKit and Trident web engines.", "Maxthon", "OmniWeb", "Opera", "Google Chrome","Maxthon")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("9","This browser was originally called Epiphany till its 3.4th version, which was released in March, 2012. The initial release was in the year 2003. It is specifically developed for the GNOME desktop by The GNOME Web Browser Developers.", "Voyager", "OmniWeb", "Lynx", "Web","Web")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("9","This web browser was developed by Google. Its beta and commercial versions were released in September 2008 for Microsoft Windows.", "Google Chrome", "Firefox", "Mozilla", "Safari","Google Chrome")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("9","It is smaller and faster than most other browsers, yet it is full- featured. Fast, user-friendly, with keyboard interface, multiple windows,  zoom functions, and more.", "Safari", "Opera", "Google Chrome", "Firefox","Opera")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("9","A web browser developed by Apple Inc. and included in Mac OS X. It was first released as a public beta in January 2003. Safari has very good support for latest technologies like XHTML, CSS2 etc.", "Internet Explorer", "Netscape", "Safari", "Mozilla","Safari")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("9","A new browser derived from Mozilla. It was released in 2004 and has grown to be the second most popular browser on the Internet.", "Konqueror", "Firefox", "Safari", "Internet Explorer","Firefox")');
    tx.executeSql('INSERT INTO quiztbl (lesson_id,question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES ("9","It is a product from software giant Microsoft. This is the most commonly used browser in the universe. This was introduced in 1995 along with Windows 95 launch and it has passed Netscape popularity in 1998.", "Konqueror", "Mozilla", "Netscape", "Internet Explorer","Internet Explorer")');



    //CONFIG
    tx.executeSql('INSERT INTO quiztbl2(lesson_id,question, qanswer1, qanswer2, answer) VALUES ("2","Step 1 is to prepare the Main board (motherboard).  Mount the CPU in the socket of the Main board. You must choose the correct CPU for your motherboard, and install it according to its instruction?", "TRUE", "FALSE","TRUE")');
    tx.executeSql('INSERT INTO quiztbl2(lesson_id,question, qanswer1, qanswer2, answer) VALUES ("2","Step 5 is to connect the CPU cooler to the Main board?", "TRUE", "FALSE","FALSE")');
    tx.executeSql('INSERT INTO quiztbl2(lesson_id,question, qanswer1, qanswer2, answer) VALUES ("2","Step 2 is to attach the Main board back plate to the case and check the Main board mounting position. The motherboard’s instructions should tell the position of the motherboard.  Install the Main board properly in its case?", "TRUE", "FALSE","FALSE")');
    tx.executeSql('INSERT INTO quiztbl2(lesson_id,question, qanswer1, qanswer2, answer) VALUES ("2","Step 4 is to Open the case and mount the power supply.  Make sure to connect all the connections to the drives and the motherboard?", "TRUE", "FALSE","TRUE")');
    tx.executeSql('INSERT INTO quiztbl2(lesson_id,question, qanswer1, qanswer2, answer) VALUES ("2","Step 3 is to attach the RAM (memory module) in its corresponding slots. Make sure the pins on the card line up with pins on the motherboard connector. Don’t get the RAM slots mixed up with PCI slots.  PCI slots are usually wider?", "TRUE", "FALSE","TRUE")');
    tx.executeSql('INSERT INTO quiztbl2(lesson_id,question, qanswer1, qanswer2, answer) VALUES ("2","Step 7 is to Mount the hard disk and connect it to the power supply and the motherboard. Connect the SATA connectors to the drives and the USB connectors and the case switches to the motherboard?", "TRUE", "FALSE","FALSE")');
    tx.executeSql('INSERT INTO quiztbl2(lesson_id,question, qanswer1, qanswer2, answer) VALUES ("2","Step 9 is to finally attach the power cable to the monitor and plug it to power outlet.  Connect keyboard, mouse and other peripherals?", "TRUE", "FALSE","TRUE")');
    tx.executeSql('INSERT INTO quiztbl2(lesson_id,question, qanswer1, qanswer2, answer) VALUES ("2","Step 7 is to connect the 20 or 24 pin ATX connector and 4 pin power supply control connector to motherboard?", "TRUE", "FALSE","TRUE")');
    tx.executeSql('INSERT INTO quiztbl2(lesson_id,question, qanswer1, qanswer2, answer) VALUES ("2","Step 8 is to mount the DVD-ROM drive.  After connecting the ATA cable to device, hook it up to power supply?", "TRUE", "FALSE","TRUE")');
    tx.executeSql('INSERT INTO quiztbl2(lesson_id,question, qanswer1, qanswer2, answer) VALUES ("2","Step 2 is to connect the CPU cooler to the Main board?", "TRUE", "FALSE","TRUE")');


    //SPECS
    tx.executeSql('INSERT INTO quiztbl2(lesson_id,question, qanswer1, qanswer2, answer) VALUES ("3","Static Ram loses its stored information in a very short even when power supply is on. D-RAMs are cheaper & lower.", "TRUE", "FALSE","FALSE")');
    tx.executeSql('INSERT INTO quiztbl2(lesson_id,question, qanswer1, qanswer2, answer) VALUES ("3","LED monitor is a flat panel display, which uses an array of light-emitting diodes as a video display", "TRUE", "FALSE","TRUE")');
    tx.executeSql('INSERT INTO quiztbl2(lesson_id,question, qanswer1, qanswer2, answer) VALUES ("3","Baby AT motherboard is being used by Pentium 3 and 4 processors.", "TRUE", "FALSE","TRUE")');
    tx.executeSql('INSERT INTO quiztbl2(lesson_id,question, qanswer1, qanswer2, answer) VALUES ("3","PATA Drives have usually 7 pins, 4 pins in pair of two for sending and receiving data and rest 3 pins are grounded", "TRUE", "FALSE","FALSE")');
    tx.executeSql('INSERT INTO quiztbl2(lesson_id,question, qanswer1, qanswer2, answer) VALUES ("3","5. LCD Monitor is the traditional, glass-screen, television-set-like monitor.", "TRUE", "FALSE","FALSE")');

    tx.executeSql('INSERT INTO quiztbl3 (lesson_id,question, qanswer1, qanswer2, qanswer3, answer) VALUES ("3","What processor called as the 4th generation released by Intel..?", "Core i5", "Core i7", "Itanium","Core i7")');
    tx.executeSql('INSERT INTO quiztbl3 (lesson_id,question, qanswer1, qanswer2, qanswer3, answer) VALUES ("3","What does AMD Stands for?", "Advanced Micro Devices", "Amplitude Modification Device", "Applied Micro Devices","Advanced Micro Devices")');
    tx.executeSql('INSERT INTO quiztbl3 (lesson_id,question, qanswer1, qanswer2, qanswer3, answer) VALUES ("3","These are all old model motherboard Low Insertion Force sockets.", "AT motherboard", "Baby AT Motherboard", "XT Motherboard", "XT Motherboard")');
    tx.executeSql('INSERT INTO quiztbl3 (lesson_id,question, qanswer1, qanswer2, qanswer3, answer) VALUES ("3","The traditional glass-screen, television-set-like monitor.", "CRT", "LCD", "LED","CRT")');
    tx.executeSql('INSERT INTO quiztbl3 (lesson_id,question, qanswer1, qanswer2, qanswer3, answer) VALUES ("3","What does LCD stands for?", "Low Cost Display", "Liquid Crystal Display", "Light Crystal Display","Liquid Crystal Display")');
    tx.executeSql('INSERT INTO quiztbl3 (lesson_id,question, qanswer1, qanswer2, qanswer3, answer) VALUES ("3","What does LED stands for?", "Light Emitting Diode", "Low Energy Detector", "Lighting Emitted Diodes","Light Emitting Diode")');

}
