
function home(){
    window.location.href = 'index.html#hardware';
}

function excel_tutorial(){
    window.location.href = 'excel.html';
}

function word_tutorial(){
    window.location.href = 'word.html';
}

function power_tutorial(){
    window.location.href = 'power.html';
}

function software_tutorial(){
    window.location.href = 'software.html';
}

function os_tutorial(){
    window.location.href = 'operating.html';
}

function browser_tutorial(){
    window.location.href = 'browser.html';
}
