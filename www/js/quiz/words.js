$(document).ready(function() {
    db.transaction(queryDB, errorCB);

    $('#btn_submit_yes').click(function(event) {
        submitquiz();
    });
});
/*$(document).on("pagebeforechange", function(e, data) {
    if (typeof data.toPage === "string") {
        if (data.toPage === "#quiz1") {
            var value = $("#list_choices1 :radio:checked").val();
            console.log(value);
            if (value === undefined || value === null) {
                alert('Please select an answer!');
                e.preventDefault();
            }
        } else if (data.toPage === "#quiz2") {
            var value = $("#list_choices2 :radio:checked").val();
            console.log(value);
            if (value === undefined || value === null) {
                alert('Please select an answer!');
                e.preventDefault();
            }
        } else if (data.toPage === "#quiz3") {
            var value = $("#list_choices3 :radio:checked").val();
            console.log(value);
            if (value === undefined || value === null) {
                alert('Please select an answer!');
                e.preventDefault();
            }
        } else if (data.toPage.indexOf("quiz4")) {
            var value = $("#list_choices4 :radio:checked").val();
            if (value === undefined || value === null) {
                alert('Please select an answer!');
                e.preventDefault();
            }
        } else if (data.toPage.indexOf("quiz5")) {
            var value = $("#list_choices5 :radio:checked").val();
            if (value === undefined || value === null) {
                alert('Please select an answer!');
                e.preventDefault();
            }
        } else if (data.toPage.indexOf("quiz6")) {
            var value = $("#list_choices6 :radio:checked").val();
            if (value === undefined || value === null) {
                alert('Please select an answer!');
                e.preventDefault();
            }
        } else if (data.toPage.indexOf("quiz7")) {
            var value = $("#list_choices7 :radio:checked").val();
            if (value === undefined || value === null) {
                alert('Please select an answer!');
                e.preventDefault();
            }
        } else if (data.toPage.indexOf("quiz8")) {
            var value = $("#list_choices8 :radio:checked").val();
            if (value === undefined || value === null) {
                alert('Please select an answer!');
                e.preventDefault();
            }
        } else if (data.toPage.indexOf("quiz9")) {
            var value = $("#list_choices9 :radio:checked").val();
            if (value === undefined || value === null) {
                alert('Please select an answer!');
                e.preventDefault();
            }
        }
    }
});
*/

function shuffle(array) {
    var tmp, current, top = array.length;

    if (top)
        while (--top) {
            current = Math.floor(Math.random() * (top + 1));
            tmp = array[current];
            array[current] = array[top];
            array[top] = tmp;
        }

    return array;
}

function queryDB(tx) {
    var value = window.localStorage.getItem("sSQL");
    console.log('sSQL: ' + value);
    tx.executeSql(value, [], querysuccessfull, errorCB);
};

function querysuccessfull(tx, result) {
    var len = result.rows.length;
    var resultArray = [];
    var questArr = [];

    for (var i = 0; i < result.rows.length; i += 1) {
        resultArray.push(result.rows.item(i));
    }

    var shuffledArray = shuffle(resultArray);

    for (var i = 0; i < shuffledArray.length; i++) {
        var quest = new Object();

        quest.No = i + 1;
        quest.page = ('quiz' + (i + 1));
        quest.ID = shuffledArray[i].ID;
        quest.question = shuffledArray[i].question;
        quest.qanswer1 = shuffledArray[i].qanswer1;
        quest.qanswer2 = shuffledArray[i].qanswer2;
        quest.qanswer3 = shuffledArray[i].qanswer3;
        quest.qanswer4 = shuffledArray[i].qanswer4;
        quest.answer = shuffledArray[i].answer;
        questArr[i] = quest;
    }
    window.localStorage['quiz'] = JSON.stringify(questArr);
    showquestion();
};

function showquestion() {
    console.log('showquestion');

    $.mobile.loading("show");

    var quiz = JSON.parse(window.localStorage['quiz'] || '{}');

    $('#res_question_1').empty();
    $('#res_question_2').empty();
    $('#res_question_3').empty();
    $('#res_question_4').empty();
    $('#res_question_5').empty();
    $('#res_question_6').empty();
    $('#res_question_7').empty();
    $('#res_question_8').empty();
    $('#res_question_9').empty();
    $('#res_question_10').empty();
    $('#res_question_11').empty();
    $('#res_question_12').empty();
    $('#res_question_13').empty();
    $('#res_question_14').empty();
    $('#res_question_15').empty();
    $('#res_question_16').empty();
    $('#res_question_17').empty();
    $('#res_question_18').empty();
    $('#res_question_19').empty();
    $('#res_question_20').empty();


    $('#res_your_1').empty();
    $('#res_your_2').empty();
    $('#res_your_3').empty();
    $('#res_your_4').empty();
    $('#res_your_5').empty();
    $('#res_your_6').empty();
    $('#res_your_7').empty();
    $('#res_your_8').empty();
    $('#res_your_9').empty();
    $('#res_your_10').empty();
    $('#res_your_11').empty();
    $('#res_your_12').empty();
    $('#res_your_13').empty();
    $('#res_your_14').empty();
    $('#res_your_15').empty();
    $('#res_your_16').empty();
    $('#res_your_17').empty();
    $('#res_your_18').empty();
    $('#res_your_19').empty();
    $('#res_your_20').empty();


    $('#res_answer_1').empty();
    $('#res_answer_2').empty();
    $('#res_answer_3').empty();
    $('#res_answer_4').empty();
    $('#res_answer_5').empty();
    $('#res_answer_6').empty();
    $('#res_answer_7').empty();
    $('#res_answer_8').empty();
    $('#res_answer_9').empty();
    $('#res_answer_10').empty();
    $('#res_answer_11').empty();
    $('#res_answer_12').empty();
    $('#res_answer_13').empty();
    $('#res_answer_14').empty();
    $('#res_answer_15').empty();
    $('#res_answer_16').empty();
    $('#res_answer_17').empty();
    $('#res_answer_18').empty();
    $('#res_answer_19').empty();
    $('#res_answer_20').empty();

    $("#questionnumber1").text("Question " + quiz[0].No + ": ");
    $("#question1").text(quiz[0].question);
    $('#list_choices1 input:radio[id=radio-choice-1]').val(quiz[0].qanswer1);
    $('#list_choices1 input:radio[id=radio-choice-2]').val(quiz[0].qanswer2);
    $('#list_choices1 input:radio[id=radio-choice-3]').val(quiz[0].qanswer3);
    $('#list_choices1 input:radio[id=radio-choice-4]').val(quiz[0].qanswer4);
    $('#qanswer1-1').text(quiz[0].qanswer1);
    $('#qanswer2-1').text(quiz[0].qanswer2);
    $('#qanswer3-1').text(quiz[0].qanswer3);
    $('#qanswer4-1').text(quiz[0].qanswer4);

    $("#questionnumber2").text("Question " + quiz[1].No + ": ");
    $("#question2").text(quiz[1].question);
    $('#list_choices2 input:radio[id=radio-choice-1]').val(quiz[1].qanswer1);
    $('#list_choices2 input:radio[id=radio-choice-2]').val(quiz[1].qanswer2);
    $('#list_choices2 input:radio[id=radio-choice-3]').val(quiz[1].qanswer3);
    $('#list_choices2 input:radio[id=radio-choice-4]').val(quiz[1].qanswer4);
    $('#qanswer1-2').text(quiz[1].qanswer1);
    $('#qanswer2-2').text(quiz[1].qanswer2);
    $('#qanswer3-2').text(quiz[1].qanswer3);
    $('#qanswer4-2').text(quiz[1].qanswer4);

    $("#questionnumber3").text("Question " + quiz[2].No + ": ");
    $("#question3").text(quiz[2].question);
    $('#list_choices3 input:radio[id=radio-choice-1]').val(quiz[2].qanswer1);
    $('#list_choices3 input:radio[id=radio-choice-2]').val(quiz[2].qanswer2);
    $('#list_choices3 input:radio[id=radio-choice-3]').val(quiz[2].qanswer3);
    $('#list_choices3 input:radio[id=radio-choice-4]').val(quiz[2].qanswer4);
    $('#qanswer1-3').text(quiz[2].qanswer1);
    $('#qanswer2-3').text(quiz[2].qanswer2);
    $('#qanswer3-3').text(quiz[2].qanswer3);
    $('#qanswer4-3').text(quiz[2].qanswer4);

    $("#questionnumber4").text("Question " + quiz[3].No + ": ");
    $("#question4").text(quiz[3].question);
    $('#list_choices4 input:radio[id=radio-choice-1]').val(quiz[3].qanswer1);
    $('#list_choices4 input:radio[id=radio-choice-2]').val(quiz[3].qanswer2);
    $('#list_choices4 input:radio[id=radio-choice-3]').val(quiz[3].qanswer3);
    $('#list_choices4 input:radio[id=radio-choice-4]').val(quiz[3].qanswer4);
    $('#qanswer1-4').text(quiz[3].qanswer1);
    $('#qanswer2-4').text(quiz[3].qanswer2);
    $('#qanswer3-4').text(quiz[3].qanswer3);
    $('#qanswer4-4').text(quiz[3].qanswer4);

    $("#questionnumber5").text("Question " + quiz[4].No + ": ");
    $("#question5").text(quiz[4].question);
    $('#list_choices5 input:radio[id=radio-choice-1]').val(quiz[4].qanswer1);
    $('#list_choices5 input:radio[id=radio-choice-2]').val(quiz[4].qanswer2);
    $('#list_choices5 input:radio[id=radio-choice-3]').val(quiz[4].qanswer3);
    $('#list_choices5 input:radio[id=radio-choice-4]').val(quiz[4].qanswer4);
    $('#qanswer1-5').text(quiz[4].qanswer1);
    $('#qanswer2-5').text(quiz[4].qanswer2);
    $('#qanswer3-5').text(quiz[4].qanswer3);
    $('#qanswer4-5').text(quiz[4].qanswer4);

    $("#questionnumber6").text("Question " + quiz[5].No + ": ");
    $("#question6").text(quiz[5].question);
    $('#list_choices6 input:radio[id=radio-choice-1]').val(quiz[5].qanswer1);
    $('#list_choices6 input:radio[id=radio-choice-2]').val(quiz[5].qanswer2);
    $('#list_choices6 input:radio[id=radio-choice-3]').val(quiz[5].qanswer3);
    $('#list_choices6 input:radio[id=radio-choice-4]').val(quiz[5].qanswer4);
    $('#qanswer1-6').text(quiz[5].qanswer1);
    $('#qanswer2-6').text(quiz[5].qanswer2);
    $('#qanswer3-6').text(quiz[5].qanswer3);
    $('#qanswer4-6').text(quiz[5].qanswer4);

    $("#questionnumber7").text("Question " + quiz[6].No + ": ");
    $("#question7").text(quiz[6].question);
    $('#list_choices7 input:radio[id=radio-choice-1]').val(quiz[6].qanswer1);
    $('#list_choices7 input:radio[id=radio-choice-2]').val(quiz[6].qanswer2);
    $('#list_choices7 input:radio[id=radio-choice-3]').val(quiz[6].qanswer3);
    $('#list_choices7 input:radio[id=radio-choice-4]').val(quiz[6].qanswer4);
    $('#qanswer1-7').text(quiz[6].qanswer1);
    $('#qanswer2-7').text(quiz[6].qanswer2);
    $('#qanswer3-7').text(quiz[6].qanswer3);
    $('#qanswer4-7').text(quiz[6].qanswer4);

    $("#questionnumber8").text("Question " + quiz[7].No + ": ");
    $("#question8").text(quiz[7].question);
    $('#list_choices8 input:radio[id=radio-choice-1]').val(quiz[7].qanswer1);
    $('#list_choices8 input:radio[id=radio-choice-2]').val(quiz[7].qanswer2);
    $('#list_choices8 input:radio[id=radio-choice-3]').val(quiz[7].qanswer3);
    $('#list_choices8 input:radio[id=radio-choice-4]').val(quiz[7].qanswer4);
    $('#qanswer1-8').text(quiz[7].qanswer1);
    $('#qanswer2-8').text(quiz[7].qanswer2);
    $('#qanswer3-8').text(quiz[7].qanswer3);
    $('#qanswer4-8').text(quiz[7].qanswer4);

    $("#questionnumber9").text("Question " + quiz[8].No + ": ");
    $("#question9").text(quiz[8].question);
    $('#list_choices9 input:radio[id=radio-choice-1]').val(quiz[8].qanswer1);
    $('#list_choices9 input:radio[id=radio-choice-2]').val(quiz[8].qanswer2);
    $('#list_choices9 input:radio[id=radio-choice-3]').val(quiz[8].qanswer3);
    $('#list_choices9 input:radio[id=radio-choice-4]').val(quiz[8].qanswer4);
    $('#qanswer1-9').text(quiz[8].qanswer1);
    $('#qanswer2-9').text(quiz[8].qanswer2);
    $('#qanswer3-9').text(quiz[8].qanswer3);
    $('#qanswer4-9').text(quiz[8].qanswer4);

    $("#questionnumber10").text("Question " + quiz[9].No + ": ");
    $("#question10").text(quiz[9].question);
    $('#list_choices10 input:radio[id=radio-choice-1]').val(quiz[9].qanswer1);
    $('#list_choices10 input:radio[id=radio-choice-2]').val(quiz[9].qanswer2);
    $('#list_choices10 input:radio[id=radio-choice-3]').val(quiz[9].qanswer3);
    $('#list_choices10 input:radio[id=radio-choice-4]').val(quiz[9].qanswer4);
    $('#qanswer1-10').text(quiz[9].qanswer1);
    $('#qanswer2-10').text(quiz[9].qanswer2);
    $('#qanswer3-10').text(quiz[9].qanswer3);
    $('#qanswer4-10').text(quiz[9].qanswer4);

    $("#questionnumber11").text("Question " + quiz[10].No + ": ");
    $("#question11").text(quiz[10].question);
    $('#list_choices11 input:radio[id=radio-choice-1]').val(quiz[10].qanswer1);
    $('#list_choices11 input:radio[id=radio-choice-2]').val(quiz[10].qanswer2);
    $('#list_choices11 input:radio[id=radio-choice-3]').val(quiz[10].qanswer3);
    $('#list_choices11 input:radio[id=radio-choice-4]').val(quiz[10].qanswer4);
    $('#qanswer1-11').text(quiz[10].qanswer1);
    $('#qanswer2-11').text(quiz[10].qanswer2);
    $('#qanswer3-11').text(quiz[10].qanswer3);
    $('#qanswer4-11').text(quiz[10].qanswer4);

    $("#questionnumber12").text("Question " + quiz[11].No + ": ");
    $("#question12").text(quiz[11].question);
    $('#list_choices12 input:radio[id=radio-choice-1]').val(quiz[11].qanswer1);
    $('#list_choices12 input:radio[id=radio-choice-2]').val(quiz[11].qanswer2);
    $('#list_choices12 input:radio[id=radio-choice-3]').val(quiz[11].qanswer3);
    $('#list_choices12 input:radio[id=radio-choice-4]').val(quiz[11].qanswer4);
    $('#qanswer1-12').text(quiz[11].qanswer1);
    $('#qanswer2-12').text(quiz[11].qanswer2);
    $('#qanswer3-12').text(quiz[11].qanswer3);
    $('#qanswer4-12').text(quiz[11].qanswer4);

    $("#questionnumber13").text("Question " + quiz[12].No + ": ");
    $("#question13").text(quiz[12].question);
    $('#list_choices13 input:radio[id=radio-choice-1]').val(quiz[12].qanswer1);
    $('#list_choices13 input:radio[id=radio-choice-2]').val(quiz[12].qanswer2);
    $('#list_choices13 input:radio[id=radio-choice-3]').val(quiz[12].qanswer3);
    $('#list_choices13 input:radio[id=radio-choice-4]').val(quiz[12].qanswer4);
    $('#qanswer1-13').text(quiz[12].qanswer1);
    $('#qanswer2-13').text(quiz[12].qanswer2);
    $('#qanswer3-13').text(quiz[12].qanswer3);
    $('#qanswer4-13').text(quiz[12].qanswer4);

    $("#questionnumber14").text("Question " + quiz[13].No + ": ");
    $("#question14").text(quiz[13].question);
    $('#list_choices14 input:radio[id=radio-choice-1]').val(quiz[13].qanswer1);
    $('#list_choices14 input:radio[id=radio-choice-2]').val(quiz[13].qanswer2);
    $('#list_choices14 input:radio[id=radio-choice-3]').val(quiz[13].qanswer3);
    $('#list_choices14 input:radio[id=radio-choice-4]').val(quiz[13].qanswer4);
    $('#qanswer1-14').text(quiz[13].qanswer1);
    $('#qanswer2-14').text(quiz[13].qanswer2);
    $('#qanswer3-14').text(quiz[13].qanswer3);
    $('#qanswer4-14').text(quiz[13].qanswer4);

    $("#questionnumber15").text("Question " + quiz[14].No + ": ");
    $("#question15").text(quiz[14].question);
    $('#list_choices15 input:radio[id=radio-choice-1]').val(quiz[14].qanswer1);
    $('#list_choices15 input:radio[id=radio-choice-2]').val(quiz[14].qanswer2);
    $('#list_choices15 input:radio[id=radio-choice-3]').val(quiz[14].qanswer3);
    $('#list_choices15 input:radio[id=radio-choice-4]').val(quiz[14].qanswer4);
    $('#qanswer1-15').text(quiz[14].qanswer1);
    $('#qanswer2-15').text(quiz[14].qanswer2);
    $('#qanswer3-15').text(quiz[14].qanswer3);
    $('#qanswer4-15').text(quiz[14].qanswer4);

    $("#questionnumber16").text("Question " + quiz[15].No + ": ");
    $("#question16").text(quiz[15].question);
    $('#list_choices16 input:radio[id=radio-choice-1]').val(quiz[15].qanswer1);
    $('#list_choices16 input:radio[id=radio-choice-2]').val(quiz[15].qanswer2);
    $('#list_choices16 input:radio[id=radio-choice-3]').val(quiz[15].qanswer3);
    $('#list_choices16 input:radio[id=radio-choice-4]').val(quiz[15].qanswer4);
    $('#qanswer1-16').text(quiz[15].qanswer1);
    $('#qanswer2-16').text(quiz[15].qanswer2);
    $('#qanswer3-16').text(quiz[15].qanswer3);
    $('#qanswer4-16').text(quiz[15].qanswer4);

    $("#questionnumber17").text("Question " + quiz[16].No + ": ");
    $("#question17").text(quiz[16].question);
    $('#list_choices17 input:radio[id=radio-choice-1]').val(quiz[16].qanswer1);
    $('#list_choices17 input:radio[id=radio-choice-2]').val(quiz[16].qanswer2);
    $('#list_choices17 input:radio[id=radio-choice-3]').val(quiz[16].qanswer3);
    $('#list_choices17 input:radio[id=radio-choice-4]').val(quiz[16].qanswer4);
    $('#qanswer1-17').text(quiz[16].qanswer1);
    $('#qanswer2-17').text(quiz[16].qanswer2);
    $('#qanswer3-17').text(quiz[16].qanswer3);
    $('#qanswer4-17').text(quiz[16].qanswer4);

    $("#questionnumber18").text("Question " + quiz[17].No + ": ");
    $("#question18").text(quiz[17].question);
    $('#list_choices18 input:radio[id=radio-choice-1]').val(quiz[17].qanswer1);
    $('#list_choices18 input:radio[id=radio-choice-2]').val(quiz[17].qanswer2);
    $('#list_choices18 input:radio[id=radio-choice-3]').val(quiz[17].qanswer3);
    $('#list_choices18 input:radio[id=radio-choice-4]').val(quiz[17].qanswer4);
    $('#qanswer1-18').text(quiz[17].qanswer1);
    $('#qanswer2-18').text(quiz[17].qanswer2);
    $('#qanswer3-18').text(quiz[17].qanswer3);
    $('#qanswer4-18').text(quiz[17].qanswer4);

    $("#questionnumber19").text("Question " + quiz[18].No + ": ");
    $("#question19").text(quiz[18].question);
    $('#list_choices19 input:radio[id=radio-choice-1]').val(quiz[18].qanswer1);
    $('#list_choices19 input:radio[id=radio-choice-2]').val(quiz[18].qanswer2);
    $('#list_choices19 input:radio[id=radio-choice-3]').val(quiz[18].qanswer3);
    $('#list_choices19 input:radio[id=radio-choice-4]').val(quiz[18].qanswer4);
    $('#qanswer1-19').text(quiz[18].qanswer1);
    $('#qanswer2-19').text(quiz[18].qanswer2);
    $('#qanswer3-19').text(quiz[18].qanswer3);
    $('#qanswer4-19').text(quiz[18].qanswer4);

    $("#questionnumber20").text("Question " + quiz[19].No + ": ");
    $("#question20").text(quiz[19].question);
    $('#list_choices20 input:radio[id=radio-choice-1]').val(quiz[19].qanswer1);
    $('#list_choices20 input:radio[id=radio-choice-2]').val(quiz[19].qanswer2);
    $('#list_choices20 input:radio[id=radio-choice-3]').val(quiz[19].qanswer3);
    $('#list_choices20 input:radio[id=radio-choice-4]').val(quiz[19].qanswer4);
    $('#qanswer1-20').text(quiz[19].qanswer1);
    $('#qanswer2-20').text(quiz[19].qanswer2);
    $('#qanswer3-20').text(quiz[19].qanswer3);
    $('#qanswer4-20').text(quiz[19].qanswer4);


    $.mobile.loading("hide");
};

function submitquiz() {
    var quiz = JSON.parse(window.localStorage['quiz'] || '{}');

    var answer1 = $("#list_choices1 :radio:checked").val();
    var trueanswer1 = quiz[0].answer;
    var question1 = quiz[0].question;

    var answer2 = $("#list_choices2 :radio:checked").val();
    var trueanswer2 = quiz[1].answer;
    var question2 = quiz[1].question;

    var answer3 = $('#list_choices3 :radio:checked').val();
    var trueanswer3 = quiz[2].answer;
    var question3 = quiz[2].question;

    var answer4 = $("#list_choices4 :radio:checked").val();
    var trueanswer4 = quiz[3].answer;
    var question4 = quiz[3].question;

    var answer5 = $("#list_choices5 :radio:checked").val();
    var trueanswer5 = quiz[4].answer;
    var question5 = quiz[4].question;

    var answer6 = $("#list_choices6 :radio:checked").val();
    var trueanswer6 = quiz[5].answer;
    var question6 = quiz[5].question;

    var answer7 = $("#list_choices7 :radio:checked").val();
    var trueanswer7 = quiz[6].answer;
    var question7 = quiz[6].question;

    var answer8 = $("#list_choices8 :radio:checked").val();
    var trueanswer8 = quiz[7].answer;
    var question8 = quiz[7].question;

    var answer9 = $("#list_choices9 :radio:checked").val();
    var trueanswer9 = quiz[8].answer;
    var question9 = quiz[8].question;

    var answer10 = $("#list_choices10 :radio:checked").val();
    var trueanswer10 = quiz[9].answer;
    var question10 = quiz[9].question;

    var answer11 = $("#list_choices11 :radio:checked").val();
    var trueanswer11 = quiz[10].answer;
    var question11 = quiz[10].question;

    var answer12 = $("#list_choices12 :radio:checked").val();
    var trueanswer12 = quiz[11].answer;
    var question12 = quiz[11].question;

    var answer13 = $('#list_choices13 :radio:checked').val();
    var trueanswer13 = quiz[12].answer;
    var question13 = quiz[12].question;

    var answer14 = $("#list_choices14 :radio:checked").val();
    var trueanswer14 = quiz[13].answer;
    var question14 = quiz[13].question;

    var answer15 = $("#list_choices15 :radio:checked").val();
    var trueanswer15 = quiz[14].answer;
    var question15 = quiz[14].question;

    var answer16 = $("#list_choices16 :radio:checked").val();
    var trueanswer16 = quiz[15].answer;
    var question16 = quiz[15].question;

    var answer17 = $("#list_choices17 :radio:checked").val();
    var trueanswer17 = quiz[6].answer;
    var question17 = quiz[6].question;

    var answer18 = $("#list_choices18 :radio:checked").val();
    var trueanswer18 = quiz[17].answer;
    var question18 = quiz[17].question;

    var answer19 = $("#list_choices19 :radio:checked").val();
    var trueanswer19 = quiz[18].answer;
    var question19 = quiz[18].question;

    var answer20 = $("#list_choices10 :radio:checked").val();
    var trueanswer20 = quiz[19].answer;
    var question20 = quiz[19].question;



    if (answer1 == trueanswer1) {
        score = score + 20;
        console.log('1 SCORE: ' + score);
    }

    if (answer2 == trueanswer2) {
        score = score + 20;
        console.log('2 SCORE: ' + score);
    }

    if (answer3 == trueanswer3) {
        score = score + 20;
        console.log('3 SCORE: ' + score);
    }

    if (answer4 == trueanswer4) {
        score = score + 20;
        console.log('4 SCORE: ' + score);
    }

    if (answer5 == trueanswer5) {
        score = score + 20;
        console.log('5 SCORE: ' + score);
    }

    if (answer6 == trueanswer6) {
        score = score + 20;
        console.log('6 SCORE: ' + score);
    }

    if (answer7 == trueanswer7) {
        score = score + 20;
        console.log('7 SCORE: ' + score);
    }

    if (answer8 == trueanswer8) {
        score = score + 20;
        console.log('8 SCORE: ' + score);
    }

    if (answer9 == trueanswer9) {
        score = score + 20;
        console.log('9 SCORE: ' + score);
    }

    if (answer10 == trueanswer10) {
        score = score + 20;
        console.log('10 SCORE: ' + score);
    }

    if (answer11 == trueanswer11) {
        score = score + 20;
        console.log('1 SCORE: ' + score);
    }

    if (answer12 == trueanswer12) {
        score = score + 20;
        console.log('2 SCORE: ' + score);
    }

    if (answer13 == trueanswer13) {
        score = score + 20;
        console.log('3 SCORE: ' + score);
    }

    if (answer14 == trueanswer14) {
        score = score + 20;
        console.log('4 SCORE: ' + score);
    }

    if (answer15 == trueanswer15) {
        score = score + 20;
        console.log('5 SCORE: ' + score);
    }

    if (answer16 == trueanswer16) {
        score = score + 20;
        console.log('6 SCORE: ' + score);
    }

    if (answer17 == trueanswer17) {
        score = score + 20;
        console.log('7 SCORE: ' + score);
    }

    if (answer18 == trueanswer18) {
        score = score + 20;
        console.log('8 SCORE: ' + score);
    }

    if (answer19 == trueanswer19) {
        score = score + 20;
        console.log('9 SCORE: ' + score);
    }

    if (answer20 == trueanswer20) {
        score = score + 20;
        console.log('10 SCORE: ' + score);
    }

    $('#game_score').text(score);

    $('#res_question_1').text(quiz[0].No + '. ' + quiz[0].question);
    $('#res_question_2').text(quiz[1].No + '. ' + quiz[1].question);
    $('#res_question_3').text(quiz[2].No + '. ' + quiz[2].question);
    $('#res_question_4').text(quiz[3].No + '. ' + quiz[3].question);
    $('#res_question_5').text(quiz[4].No + '. ' + quiz[4].question);
    $('#res_question_6').text(quiz[5].No + '. ' + quiz[5].question);
    $('#res_question_7').text(quiz[6].No + '. ' + quiz[6].question);
    $('#res_question_8').text(quiz[7].No + '. ' + quiz[7].question);
    $('#res_question_9').text(quiz[8].No + '. ' + quiz[8].question);
    $('#res_question_10').text(quiz[9].No + '. ' + quiz[9].question);
    $('#res_question_11').text(quiz[10].No + '. ' + quiz[10].question);
    $('#res_question_12').text(quiz[11].No + '. ' + quiz[11].question);
    $('#res_question_13').text(quiz[12].No + '. ' + quiz[12].question);
    $('#res_question_14').text(quiz[13].No + '. ' + quiz[13].question);
    $('#res_question_15').text(quiz[14].No + '. ' + quiz[14].question);
    $('#res_question_16').text(quiz[15].No + '. ' + quiz[15].question);
    $('#res_question_17').text(quiz[16].No + '. ' + quiz[16].question);
    $('#res_question_18').text(quiz[17].No + '. ' + quiz[17].question);
    $('#res_question_19').text(quiz[18].No + '. ' + quiz[18].question);
    $('#res_question_20').text(quiz[19].No + '. ' + quiz[19].question);

    $('#res_your_1').text($("#list_choices1 :radio:checked").val());
    $('#res_your_2').text($("#list_choices2 :radio:checked").val());
    $('#res_your_3').text($("#list_choices3 :radio:checked").val());
    $('#res_your_4').text($("#list_choices4 :radio:checked").val());
    $('#res_your_5').text($("#list_choices5 :radio:checked").val());
    $('#res_your_6').text($("#list_choices6 :radio:checked").val());
    $('#res_your_7').text($("#list_choices7 :radio:checked").val());
    $('#res_your_8').text($("#list_choices8 :radio:checked").val());
    $('#res_your_9').text($("#list_choices9 :radio:checked").val());
    $('#res_your_10').text($("#list_choices10 :radio:checked").val());
    $('#res_your_11').text($("#list_choices11 :radio:checked").val());
    $('#res_your_12').text($("#list_choices12 :radio:checked").val());
    $('#res_your_13').text($("#list_choices13 :radio:checked").val());
    $('#res_your_14').text($("#list_choices14 :radio:checked").val());
    $('#res_your_15').text($("#list_choices15 :radio:checked").val());
    $('#res_your_16').text($("#list_choices16 :radio:checked").val());
    $('#res_your_17').text($("#list_choices17 :radio:checked").val());
    $('#res_your_18').text($("#list_choices18 :radio:checked").val());
    $('#res_your_19').text($("#list_choices19 :radio:checked").val());
    $('#res_your_20').text($("#list_choices20 :radio:checked").val());

    $('#res_answer_1').text(quiz[0].answer);
    $('#res_answer_2').text(quiz[1].answer);
    $('#res_answer_3').text(quiz[2].answer);
    $('#res_answer_4').text(quiz[3].answer);
    $('#res_answer_5').text(quiz[4].answer);
    $('#res_answer_6').text(quiz[5].answer);
    $('#res_answer_7').text(quiz[6].answer);
    $('#res_answer_8').text(quiz[7].answer);
    $('#res_answer_9').text(quiz[8].answer);
    $('#res_answer_10').text(quiz[9].answer);
    $('#res_answer_11').text(quiz[10].answer);
    $('#res_answer_12').text(quiz[11].answer);
    $('#res_answer_13').text(quiz[12].answer);
    $('#res_answer_14').text(quiz[13].answer);
    $('#res_answer_15').text(quiz[14].answer);
    $('#res_answer_16').text(quiz[15].answer);
    $('#res_answer_17').text(quiz[16].answer);
    $('#res_answer_18').text(quiz[17].answer);
    $('#res_answer_19').text(quiz[18].answer);
    $('#res_answer_20').text(quiz[19].answer);


    $.mobile.changePage($("#result"));

    setTimeout(function() {
      $('#popupScore').popup('open');
    }, 1000);
};
